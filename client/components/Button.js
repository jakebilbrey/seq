var React = require('react');

var Button = React.createClass({
	render: function() {
		return (
			<div className="Button" {...this.props}>
        {this.props.children}
			</div>
		);
	}
});

module.exports = Button;
