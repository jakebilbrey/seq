var React = require('react');

var ProductDialog = React.createClass({
	getDefaultProps: function() {
		return {
      product: {},
      onCloseButtonClick: function() {},
		}
	},

  _onCloseButtonClick: function() {
    this.props.onCloseButtonClick();
  },

	render: function() {
		return (
			<div className='ProductDialog'>
        <div
          className='ProductDialog-close'
          onClick={this._onCloseButtonClick}
        >
          <i className="fa fa-times"></i>
        </div>

          <p>{this.props.product.name}</p>
          <p>{this.props.product.description}</p>
			</div>
		);
	}
});

module.exports = ProductDialog;
