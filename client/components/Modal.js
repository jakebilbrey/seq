var React = require('react');

// Constants
var DEFAULT_TOP_MARGIN = 50;
var MAX_HEIGHT = 475;
var MAX_WIDTH = 860;

var Modal = React.createClass({
	getInitialState: function() {
		return {
      height: MAX_HEIGHT,
			marginTop: DEFAULT_TOP_MARGIN,
      width: MAX_WIDTH,
		};
	},

	componentWillMount: function() {
		this._onBrowserResize();
	},

	componentDidMount: function() {
		window.addEventListener('resize', this._onBrowserResize);
	},

	componentWillUnmount: function() {
		window.removeEventListener('resize', this._onBrowserResize);
	},

  _onBrowserResize: function() {
    var marginTop = (window.innerHeight - 475) / 2;
		if (marginTop < 0) {
			marginTop = 0;
		}

    var width = MAX_WIDTH;
    if (window.innerWidth < MAX_WIDTH) {
      width = window.innerWidth;
    }

		var height = MAX_HEIGHT;
    if (window.innerHeight < MAX_HEIGHT) {
      height = window.innerHeight;
    }

    this.setState({
      height: height,
      marginTop: marginTop,
      width: width,
    });
  },

	render: function() {
		return (
			<div className='Modal'>
        <div
          className='Modal-dialog'
          style={{
            height: this.state.height,
            marginTop: this.state.marginTop,
            width: this.state.width,
          }}
        >
				  {this.props.children}
        </div>
			</div>
		);
	}
});

module.exports = Modal;
