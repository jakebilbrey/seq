var React = require('react');

var Carousel = React.createClass({
	getInitialState: function() {
		var hasNext = true;
		var hasPrev = true;
		if (this.props.currentProduct === this.props.productsCount-1) {
			hasNext = false;
		}
		if (this.props.currentProduct === 0) {
			hasPrev = false;
		}

		return {
			hasNext: hasNext,
			hasPrev: hasPrev,
			scrollX: 0,
		};
	},

	getDefaultProps: function() {
		return {
			currentProduct: 0,
			productsCount: 0,
			updateCurrentProduct: function() {},
		}
	},

	componentDidUpdate: function(prevProps, prevState, prevContext) {
    if (this.props.currentProduct !== prevProps.currentProduct) {
      this._updateButtons();
			this._scrollCarousel();
    }
  },

	_renderDots: function() {
		var output = [];
		for (var i = 0; i < this.props.productsCount; i++) {
			var selected = '';
			if (i === this.props.currentProduct) {
				selected = 'Carousel-dot-selected';
			}
			output.push(
				<div
					className={'Carousel-dot ' + selected}
					key={i}
					onClick={this._onDotClick.bind(this, i)}
				/>
			);
		}
		return output;
	},

	_onDotClick: function(i) {
		this.props.updateCurrentProduct(i);
	},

	_updateButtons: function() {
		var hasNext = true;
		var hasPrev = true;
		if (this.props.currentProduct === this.props.productsCount-1) {
			hasNext = false;
		}
		if (this.props.currentProduct === 0) {
			hasPrev = false;
		}

		this.setState({
			hasNext: hasNext,
			hasPrev: hasPrev,
		});
	},

	_onNextClick: function() {
		var newCurrentIndex = this.props.currentProduct + 1;
		if (newCurrentIndex < this.props.productsCount) {
			this.props.updateCurrentProduct(newCurrentIndex);
		}
	},

	_onPrevClick: function() {
		var newCurrentIndex = this.props.currentProduct - 1;
		if (newCurrentIndex >= 0) {
			this.props.updateCurrentProduct(newCurrentIndex);
		}
	},

	_updateCurrentProduct: function(newCurrentIndex) {
		this.props.updateCurrentProduct(newCurrentIndex);
	},

	_scrollCarousel: function() {
		var width = 860; // this should be smarter
		var screenWidth = window.innerWidth;
		if (screenWidth < width) {
			width = screenWidth;
		}
		var offset = width * (this.props.currentProduct);
		this.refs.scrollContainer.getDOMNode().scrollLeft = offset;
	},

	render: function() {
		return (
			<div className='Carousel'>
				{this.state.hasPrev ? (
					<div
						className='Carousel-prevButton'
						onClick={this._onPrevClick}
					>
						<i className='fa fa-chevron-left'></i>
					</div>
				) : null}
				{this.state.hasNext ? (
					<div
						className='Carousel-nextButton'
						onClick={this._onNextClick}
					>
						<i className='fa fa-chevron-right'></i>
					</div>
				) : null}
				<div className='Carousel-dot-container'>
					{this._renderDots()}
				</div>
				<div ref='scrollContainer' className='Carousel-scrollContainer'>
        	{this.props.children}
				</div>
			</div>
		);
	}
});

module.exports = Carousel;
