var React = require('react');

// Constants
var IMAGE_URL = 'http://seq-full-stack-assessment.s3-website-us-west-2.amazonaws.com/assets';
var MAX_WIDTH = 860;
var MAX_HEIGHT = 573;

var CarouselItem = React.createClass({
	getInitialState: function() {
		return {
			height: MAX_HEIGHT,
			width: MAX_WIDTH,
		};
	},

	getDefaultProps: function() {
		return {
      product: {}
		}
	},

	componentWillMount: function() {
		this._onBrowserResize();
	},

	componentDidMount: function() {
		window.addEventListener('resize', this._onBrowserResize);
	},

	componentWillUnmount: function() {
		window.removeEventListener('resize', this._onBrowserResize);
	},

	_onBrowserResize: function() {
		var width = MAX_WIDTH;
		var height = MAX_HEIGHT;
		var screenWidth = window.innerWidth;

		if (screenWidth < MAX_WIDTH) {
			var ratio = screenWidth / MAX_WIDTH;
			width = screenWidth;
			height = Math.floor(MAX_HEIGHT * ratio);
		}

		this.setState({
			height: height,
			width: width,
		});
	},

	render: function() {
		return (
			<div
        className='CarouselItem'
        style={{
						width: this.state.width,
						height: this.state.height,
            backgroundImage: 'url(' + IMAGE_URL +
							this.props.product.images[0]['main-view']+')'
        }}
      >
				<div className='CarouselItem-model'>
          {this.props.product.name.match(/Model No\. \d\d/i)}
        </div>
        <div className='CarouselItem-name'>
          {this.props.product.name.replace(/Model No\. \d\d\s/gi, '')}
        </div>
			</div>
		);
	}
});

module.exports = CarouselItem;
