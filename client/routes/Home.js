var React = require('react');
var Button = require('../Components/Button');
var Carousel = require('../Components/Carousel');
var CarouselItem = require('../Components/CarouselItem');
var Modal = require('../Components/Modal');
var ProductDialog = require('../Components/ProductDialog');

var Home = React.createClass({
	getInitialState: function() {
		return {
			currentProduct: 0,
			products: [],
			showModal: false,
		};
	},

	componentDidMount: function() {
		this._getProducts();
	},

	_getProducts: function() {
		function success() {
		  if (request.status >= 200 && request.status < 400) {
		    var data = JSON.parse(request.responseText).products;
		    this.setState({
		      products: data,
		    });
		  } else {
		    // Call returns error
		  }
		}

		var request = new XMLHttpRequest();
		request.open('GET', '/api/products', true);
		request.onerror = function() {
			// Could not make call
		};
		request.addEventListener('load', success.bind(this), false);
		request.send();
	},

	_renderProducts: function() {
		var products = this.state.products;
		var productsCount = products.length;
		var output = [];

		for (var i = 0; i < productsCount; i++) {
			var product = products[i];
			output.push(
				<CarouselItem
					key={product.id}
					product={product}
					ref={i}
				/>
			);
		}

		return output;
	},

	_toggleModal: function() {
		this.setState({
			showModal: !this.state.showModal,
		});
	},

	_updateCurrentProduct: function(newProductIndex) {
		this.setState({
			currentProduct: newProductIndex,
		});
	},

	render: function() {
		return (
			<div>
				<Carousel
					currentProduct={this.state.currentProduct}
					productsCount={this.state.products.length}
					updateCurrentProduct={this._updateCurrentProduct}
				>
					{this._renderProducts()}
				</Carousel>

				<div className='u-center'>
					<Button onClick={this._toggleModal}>
						Quick View
					</Button>
				</div>

				{this.state.showModal ? (
					<Modal>
						<ProductDialog
							product={this.state.products[this.state.currentProduct]}
							onCloseButtonClick={this._toggleModal}
						/>
					</Modal>
				) : null}
			</div>
		);
	}
});

module.exports = Home;
