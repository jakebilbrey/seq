var express = require('express');
var router = express.Router();
var request = require('request');

router.get('/', function(req, res) {
  res.setHeader('Content-Type', 'application/json');
  var uri = 'http://seq-front-end-assessment.s3-website-us-west-2.amazonaws.com/catalog.json';
  request(uri, function(err, resp, body) {
    var json = {};
    if (body) {
      json = JSON.parse(body);
    }
    res.send(json);
  });
});

module.exports = router;
